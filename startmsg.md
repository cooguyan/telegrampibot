*树莓派命令* 
 /getinfo :获取树莓派的型号信息 
 /getload :获取树莓派的负载情况 
 /autostart : 设定程序是否随系统启动
 /reboot : 重启树莓派 
 /poweroff : 关机 

*桌面时钟设置* 
 /setslogan :设置时钟的标语 
 /setbackcolor :设置背景颜色 如#4d1018 
 /closeclock : 关闭时钟 
 /openclock :打开时钟，这会占用整个屏幕 
 /fontsize :调整时钟字体大小
 /slfontsize : 调整标语字体大小