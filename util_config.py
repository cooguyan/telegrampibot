#配置文件的生成修改和上传
import os
import logging
from ruamel import yaml
from qiniu import Auth, BucketManager, CdnManager, etag, put_file
home = os.environ['HOME']
dir_path = home+"/.telegrampibot/"
config_path = dir_path+"config.yaml"
logger = logging.getLogger()
def init_config():
    """初始化配置文件"""

        
    if os.path.exists(config_path):
        logger.info("检测到配置文件，跳过")
    else:
        alldata = {'qiniu': 
            {'access_key': 'access key', 
            'cdn_url': 'http(s)://xxxxxxxx', 
            'bucket_name': 'bucket name',
            'secret_key': 'secret key'}, 
        'telegrambot': 
            {'bot_token': 'bot token', 
            'proxy_on':False,
            'proxy_url': 'proxy url'}, 
        'theclock': 
            {'backcolor': '#142334', 
            'slogan': '加油！你是最胖的！',
            'fontsize':'150px',
            'slogan_fontsize':'55px',
            'browser_flag':False}
        }
        with open(config_path, 'w+', encoding='utf8') as outfile:
            yaml.dump(alldata, outfile, default_flow_style=False, allow_unicode=True)
            logger.info("配置文件初始化完成")

def read_config(fkey,ckey):
    """读取配置文件"""

    #读取文件
    with open(config_path,'r',encoding='utf-8') as cfg:
        try:
            alldata = yaml.safe_load(cfg)
            logger.info("配置文件读取成功")
            return alldata[fkey][ckey]
        except yaml.YAMLError as e:
            logger.exception(e)


def update_config(fkey,ckey,value):
    """更新配置文件"""
    """fkey:父级key; ckey:子级key;value"""
    alldata = {}
    #读取文件
    with open(config_path,'r',encoding='utf-8') as cfg:

        try:
            alldata = yaml.safe_load(cfg)
            alldata[fkey][ckey] = value
            logger.info("配置文件读取成功")
        except yaml.YAMLError as e:
            logger.exception(e)

    # 写入文件
    with open(config_path,'w+',encoding='utf-8') as cfg :
        yaml.dump(alldata,cfg, default_flow_style=False, allow_unicode=True)
        logger.info("配置文件修改成功")

def upload_config():
    """上传配置文件"""
    # 构建鉴权对象
    ak=read_config("qiniu","access_key")
    sk=read_config("qiniu","secret_key")

    q = Auth(access_key=ak, secret_key=sk)
    # 要上传的空间
    bucket_name = read_config("qiniu","bucket_name")
    # 上传后保存的文件名
    key = 'config.yaml'
    # 初始化BucketManager
    bucket = BucketManager(q)
    # 删除bucket_name 中的文件 key
    info = bucket.delete(bucket_name, key)
    logger.info(info)
    # 生成上传 Token，可以指定过期时间等
    token = q.upload_token(bucket_name, key, 3600)
    # 要上传文件的本地路径
    info = put_file(token, key, config_path)
    logger.info(info)
    return

def download_config():
    """下载配置文件"""
    #思路是通过手机发送请求进行下载，自己手动导入
    return