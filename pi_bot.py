import logging
import os
import re
import subprocess
import sys
import time
from multiprocessing import Process
from telegram.error import NetworkError, InvalidToken,Unauthorized
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater

import util_config
import util_raspi

logger = logging.getLogger()

# TODO 将命令进一步包装，更多的利用按钮来实现交互

# 命令handler
def start(update, context):
    """功能展示"""
    # 读取文件并发送，这样可以比较方便的编辑消息
    util_raspi.simulated_click()
    with open('/home/pi/telegrampibot/startmsg.md', 'r') as f:
        update.message.reply_markdown(f.read())


def close_clock(update, context):

    """关闭时钟"""
    util_raspi.close_browser()
    update.message.reply_text("正在关闭时钟🤖")
    return

def open_clock(update, context):
    """打开时钟"""
    util_raspi.open_browser()
    update.message.reply_text("正在打开时钟🤖")
    return


def set_slogan(update, context):
    """修改树莓派时钟的标语"""
    try:
        # args[0] should contain the time for the timer in seconds
        new_slogan = str(context.args[0])  # 获取标语
        if new_slogan.strip() == "":
            update.message.reply_text('并没有输入slogan')
            return
        #更新配置文件
        util_config.update_config("theclock","slogan", new_slogan)

        # 模拟鼠标点击，触发slogan的更新
        util_raspi.simulated_click()
        
    except (IndexError, ValueError):
        update.message.reply_text('/setslogan <标语正文>')
        util_raspi.simulated_click()

def set_backcolor(update,context):
    """修改背景颜色"""
    try:
        # args[0] should contain the time for the timer in seconds
        new_color = str(context.args[0])  # 获取色彩编码
        if new_color.strip() == "":
            update.message.reply_text('输入为空')
            return
        if re.match("^#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})$",new_color):

            util_config.update_config("theclock","backcolor", new_color)
            # 模拟目标点击，触发slogan的更新
            util_raspi.simulated_click()
            # 将配置上传备份
            #util_config.upload_config() 
            #update.message.reply_markdown("🤖配置已上传至 [七牛云](https://portal.qiniu.com/kodo/overview) 备份")
        else:
            update.message.reply_text('格式应为：#xxzsss')

        


    except (IndexError, ValueError):
        update.message.reply_text('/setbackcolor <#xxxxxx>')
        util_raspi.simulated_click()

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def get_info(update,context):
    """获取树莓派型号信息"""  
    try:
        #树莓派版本型号
        rpimodel= util_raspi.getmodel()
        #发行版信息
        rpirelease = util_raspi.getreleaseinfo()
        update.message.reply_markdown(rpimodel+"\n"+rpirelease)
    except Exception as e:
        logger.exception(e)
        update.message.reply_text(e)
 
def get_load(update,context):
    """获取树莓派的负载"""
    try:
        cpu_use = util_raspi.getCPULoad()
        cpu_temp = util_raspi.getCPUtemperature()
        ram_info = util_raspi.getRAMinfo()
        disk_space = util_raspi.getDiskinfo()
        update.message.reply_markdown("🤖"+"\n*CPU信息：*\n"+cpu_use+"\n*内存信息：*\n"+ ram_info+"\n*磁盘信息：*\n"+disk_space+"\n*温度信息：*\n"+cpu_temp)
    except Exception as e:
        logger.exception(e)

def rpi_reboot(update,context):
    """重启树莓派"""
    #TODO 添加一个确认按钮，避免误触
    update.message.reply_text("重启中🤖")
    


def rpi_poweroff(update,context):
    """关闭树莓派"""
    #TODO 添加一个确认按钮，避免误触

    update.message.reply_text("关机中🤖")
    util_raspi.poweroff()

def autostart(update,context):
    auto = int(context.args[0])
    if auto==0:
        util_raspi.set_autostart(autostart=False)
        update.message.reply_text("🤖已取消开机自启")

    else:
        util_raspi.set_autostart()
        update.message.reply_text("🤖bot将开机自启")


def download_config(update,context):
    update.message.reply_text("下载配置文件🤖")

def fontsize_update(update,context):
    fs = str(context.args[0])
    util_config.update_config("theclock","fontsize",fs)
    util_raspi.simulated_click()
    update.message.reply_text("时钟字体修改完成🤖")

def slogan_fontsize_update(update,context):

    slfs = str(context.args[0])
    util_config.update_config("theclock","slogan_fontsize",slfs)
    util_raspi.simulated_click() 
    update.message.reply_text("标语字体修改完成🤖")

def run_bot():
    # 在代理模式下运行，这很重要。但我不明白为什么要用socks5h,少了h会不起作用
    # 判断是否使用代理
    bot_token = util_config.read_config("telegrambot","bot_token")
    dp =None
    try:

        if util_config.read_config("telegrambot","proxy_on"):

            REQUEST_KWARGS = {
                'proxy_url': 'xxxxxxxx'}
        
            updater = Updater(bot_token, use_context=True,request_kwargs=REQUEST_KWARGS)
            # Get the dispatcher to register handlers
            dp = updater.dispatcher
        else:
            updater = Updater(bot_token,use_context=True)
            dp = updater.dispatcher
        

        dp.add_handler(CommandHandler("start", start))
        dp.add_handler(CommandHandler("setslogan", set_slogan))
        dp.add_handler(CommandHandler("setbackcolor", set_backcolor))
        dp.add_handler(CommandHandler("closeclock", close_clock))
        dp.add_handler(CommandHandler("openclock", open_clock))
        dp.add_handler(CommandHandler("getinfo", get_info))
        dp.add_handler(CommandHandler("getload", get_load))
        dp.add_handler(CommandHandler("reboot", rpi_reboot))
        dp.add_handler(CommandHandler("poweroff", rpi_poweroff))
        dp.add_handler(CommandHandler("download_config", download_config))
        dp.add_handler(CommandHandler("autostart", autostart))
        dp.add_handler(CommandHandler("fontsize", fontsize_update))
        dp.add_handler(CommandHandler("slfontsize", slogan_fontsize_update))
        dp.add_error_handler(error)

        updater.start_polling(timeout=30)

        updater.idle()

    except NetworkError as e:
        logger.exception("网络错误："+e.message)
        run_bot()
        logger.exception("正在重试 ")
    except InvalidToken  as e:   
        logger.exception("Token不合法:"+e.message)
        sys.exit()
    except Unauthorized as e:
        logger.exception("身份认证失败："+e.message)
        sys.exit()
        




    # 命令列表

if __name__ == '__main__':
    run_bot()
