# 与树莓派相关的工具方法，主要是用于代为执行一些系统命令
import os
import logging
import subprocess
import pymouse
import time
from multiprocessing import Process
from gpiozero import Button
import util_config
logger = logging.getLogger()

poweroff_button = Button(18) #关机按钮
browser_button = Button(17) # 关闭界面


def poweroff():
    """关机"""
    subprocess.Popen('sudo poweroff',shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)

def reboot():
    """重启"""
    subprocess.Popen('sudo reboot',shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)

def poweroff_button_listen():
    
    while True:
        if poweroff_button.is_pressed:
            logger.info("树莓派关机")
            poweroff()
        else:
            time.sleep(2)
def browser_button_listen():
    
    while True:
        if browser_button.is_pressed:
            browser_is_open=int(util_config.read_config("theclock","browser_flag"))
            if browser_is_open:
                close_browser()
                logger.info("浏览器已关闭")
            else:
                open_browser()
                logger.info("浏览器已打开") 
        else:
            time.sleep(2) 


def getreleaseinfo():
    """
    示例：系统信息：
    XXXX
    """
    info = _getreleaseinfo()
    result = "\n*系统版本：\n\n*"
    for i in info :
        result = result+i
    return result

def _getreleaseinfo():
    """获取发行版信息"""
    res = subprocess.Popen('lsb_release -a',shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
    result = res.stdout.readlines()
    for index in range(len(result)):
        result[index] = result[index].decode()
    return result
def getmodel():
    """返回markdown格式的树莓派型号信息"""
    info = _getmodel()
    result = "*🤖树莓派型号：*\n\n"+str(info[0],encoding='utf8')
    return result

def _getmodel():
    """获取树莓派型号""" 

    res = subprocess.Popen('cat /proc/device-tree/model',shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
    result = res.stdout.readlines()
    return result

def getCPUtemperature():
    """
    返回Markdown格式的CPU温度消息
    实例：CPU温度：* 60 * 度
    
    """
    temp =_getCPUtemperature()
    result = "\nCPU温度：*"+str(temp)+"* 度"

    return result

def _getCPUtemperature():
    """获取CPU温度"""
    cmd = "cat /sys/class/thermal/thermal_zone*/temp"
    temp = subprocess.check_output(cmd, shell = True )
    result = bytes(temp).decode()
    temp = round(float(result.split('\n')[0])/1000,2)    
    return temp

def getCPULoad():
    result = "\n负载：*"+str(_getCPULoad())+"*\n"
    return result

def _getCPULoad():
    """获取CPU负载"""
    cmd = "top -bn1 | grep load | awk '{printf \"%.2f\", $(NF-2)}'"
    CPU = subprocess.check_output(cmd, shell = True )
    result = bytes(CPU).decode()
    return result

def getDiskinfo():

    info = _getDiskinfo()
    total = str(info[0])
    used = str(info[1])
    free = str(info[2])
    rate = str(info[3])
    result = "\n容量：*"+total+"*\n已用：*"+used+"*\n可用：*"+free+"*\n比率：*"+rate+"*\n"
    return result



def _getDiskinfo():
    """获取磁盘信息"""
    p = os.popen("df -h /")
    i = 0
    while 1:
        i = i +1
        line = p.readline()
        if i==2:
            return(line.split()[1:5])

def getRAMinfo():
    info = _getRAMinfo()
    total = "\n容量：*"+StrOfSize(int(info[0]))+"*"
    used = "\n已用：*"+StrOfSize(int(info[1]))+"*"
    free = "\n可用：*"+StrOfSize(int(info[2]))+"*\n"
    result =total+used+free
    return result


def _getRAMinfo():
    """获取内存信息"""
    p = os.popen('free -b')
    i = 0
    info=[]
    while 1:
        i = i + 1
        line = p.readline()
        if i==2:
            info=line.split()[1:4]
            break
    return info

def simulated_click(x=50, y=50):
    """模拟点击屏幕"""
    try:
        m = pymouse.PyMouse()
        m.click(x, y, n=1)
    except Exception as e:
        print(e)

def close_tornado(port=8888):
    """关掉tornado服务器,参数为监听的端口"""
    command = '''kill -9 $(netstat -nlp | grep :''' + str(
        port) + ''' | awk '{print $7}' | awk -F"/" '{ print $1 }')'''
    subprocess.call(command, shell=True)
    return


def close_browser():
    """单独关掉树莓派的浏览器，便于进行其他操作"""
    chromium_kill = "pkill -f chromium-browser"
    subprocess.call(chromium_kill, shell=True)
    util_config.update_config("theclock","browser_flag",0)
    return


def open_browser():
    """全屏打开树莓派浏览器"""
    # 关闭屏幕休眠

    chromium_start = ("chromium-browser  "
                      "--disable-popup-blocking "
                      "--no-first-run "
                      "--disable-desktop-notifications  "
                      "--kiosk  http://127.0.0.1:8888")
    # subprocess.call(chromium_start,shell=True)
    subprocess.call(chromium_start, shell=True)
    util_config.update_config("theclock","browser_flag",1)
    #p = Process(target=lambda: subprocess.call(chromium_start, shell=True))
    #p.start()

def set_autostart(autostart=True):
    """设置自动启动"""
    """通过将desktop文件放进autostart文件夹实现"""
    home = os.environ['HOME']
    dir_path = home+"/.config/autostart/"
    file_path = dir_path+"telegrampibot.desktop"
    file_data =( "[Desktop Entry]\n"+
    "Name=telegrampibot\n"+
    "Comment=my bot\n"+
    "Exec=python3 /home/pi/telegrampibot/main.py\n"+
    "Icon=/home/pi/Pictures/bot.png\n"+
    "Terminal=true\n"+
    "MultipleArgs=false\n"+
    "Type=Application\n"+
    "Categories=Application;Development;\n"+
    "StartupNotify=true\n"+
    "X-KeepTerminal=true\n"+
    "Path=/home/pi/telegrampibot\n")
    #如果选择打开，那么就添加文件，如果选择关闭就把文件删除

                
    if os.path.exists(dir_path):
        logger.info("autostart目录存在，跳过")
    else:
        os.mkdir(dir_path)
        logger.info("已创建autostart目录")

    if autostart:
        with open(file_path,'w',encoding="utf-8") as f:
            f.write(file_data)
            logger.info("已添加desktop文件")
            return True
    else:
        try:
            os.remove(file_path)
            logger.info("desktop文件已删除") 
            return False
        except FileNotFoundError:
            logger.info("desktop文件不存在，跳过") 
            return False

def StrOfSize(size):
    '''
    auth: wangshengke@kedacom.com ；科达柯大侠
    递归实现，精确为最大单位值 + 小数点后三位
    '''
    def strofsize(integer, remainder, level):
        if integer >= 1024:
            remainder = integer % 1024
            integer //= 1024
            level += 1
            return strofsize(integer, remainder, level)
        else:
            return integer, remainder, level

    units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    integer, remainder, level = strofsize(size, 0, 0)
    if level+1 > len(units):
        level = -1
    return ( '{}.{:>03d} {}'.format(integer, remainder, units[level]) )

if __name__ == '__main__': 
    #print(getmodel())
    print(_getreleaseinfo())
    print(getRAMinfo())
    print(_getCPUtemperature())
    print(_getCPULoad())
    print( _getDiskinfo())
    print(getDiskinfo())

    set_autostart(False)