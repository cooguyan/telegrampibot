# -*- coding: utf-8 -*-
import json
import logging
import os
import pathlib
import subprocess
import time
from multiprocessing import Process

from ruamel import yaml

import pi_bot
import the_clock
import util_config
import util_raspi

# Enable logging
home = os.environ['HOME']
dir_path = home+"/.telegrampibot/"
if os.path.exists(dir_path):
    pass
else:
    os.mkdir(dir_path)

logging.basicConfig(level=logging.INFO,#控制台打印的日志级别
                    filename=dir_path+'telegrampibot.log',
                    filemode='a',##模式，有w和a，w就是写模式，每次都会重新写日志，覆盖之前的日志
                    #a是追加模式，默认如果不写的话，就是追加模式
                    format=
                    '%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'
                    #日志格式
                    )

logger = logging.getLogger(__name__)

def main():
    """主程序"""
    #配置文件初始化
    util_config.init_config()
    #确认没有老进程的残留
    util_raspi.close_tornado()
    util_raspi.close_browser()
    poweroff_button_listen_process = Process(target=util_raspi.poweroff_button_listen)
    brower_button_listen_process = Process(target=util_raspi.browser_button_listen)
    tornado_process = Process(target=the_clock.main)
    bot_process = Process(target=pi_bot.run_bot)
    
    bot_process.start()
    tornado_process.start()
    brower_button_listen_process.start()
    poweroff_button_listen_process.start()
    
    util_raspi.open_browser()
    logger.warning("main的pid为"+str(os.getpid()))
    logger.warning("bot的pid为"+str(bot_process.pid))
    
if __name__ == '__main__':
    main()
