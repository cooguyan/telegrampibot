import asyncio
import tornado.escape
import tornado.ioloop
import tornado.locks
import tornado.web
import os.path
import uuid
import requests
from tornado.options import define, options, parse_command_line
import logging 
import util_config
logging.getLogger("requests").setLevel(logging.WARNING)
define("port", default=8888, help="run on the given port", type=int)
define("debug", default=True, help="run in debug mode")


class MessageBuffer(object):
    def __init__(self):
        # cond is notified whenever the message cache is updated
        self.cond = tornado.locks.Condition()
        self.cache = []
        self.cache_size = 200

    def get_messages_since(self, cursor):
        """Returns a list of messages newer than the given cursor.
        ``cursor`` should be the ``id`` of the last message received.
        """
        results = []
        for msg in reversed(self.cache):
            if msg["id"] == cursor:
                break
            results.append(msg)
        results.reverse()
        return results

    def add_message(self, message):
        self.cache.append(message)
        if len(self.cache) > self.cache_size:
            self.cache = self.cache[-self.cache_size :]
        self.cond.notify_all()


global_message_buffer = MessageBuffer()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("clock.html", messages=global_message_buffer.cache)



class SloganUpdatesHandler(tornado.web.RequestHandler):
    """更新slogan"""
    async def get(self):
        slogan = util_config.read_config("theclock","slogan")
        self.write(slogan)
class BackColorUpdatesHandler(tornado.web.RequestHandler):
    """更新背景颜色"""
    async def get(self):
        backcolor = util_config.read_config("theclock","backcolor")
        self.write(backcolor)
class FontSizeUpdatesHandler(tornado.web.RequestHandler):
    """更新页面字体大小"""
    async def get(self):
        font_size = util_config.read_config("theclock","fontsize")
        self.write(font_size)
class SL_FontSizeUpdatesHandler(tornado.web.RequestHandler):
    """更新slogan字体大小"""
    async def get(self):
        font_size = util_config.read_config("theclock","slogan_fontsize")
        self.write(font_size)
def main():
    parse_command_line() #
    app = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/a/slogan/updates", SloganUpdatesHandler),
            (r"/a/backcolor/updates", BackColorUpdatesHandler),
            (r"/a/fontsize/updates", FontSizeUpdatesHandler),
            (r"/a/slogan_fontsize/updates", SL_FontSizeUpdatesHandler),
        ],
        cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        xsrf_cookies=True,
        debug=options.debug,
    )
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()