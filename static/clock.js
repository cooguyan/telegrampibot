((doc) => {
    function createTextSetter(selector) {
        const elem = doc.querySelector(selector);
        let text;
        return newText => {
            if (newText !== text) {
                text = newText;
                elem.textContent = newText;
                return true;
            }
            return false;
        };
    }
document.addEventListener('click',loadstyle);

function loadstyle(e){
    $.get("/a/slogan/updates",function(data,status){
        var sty = data
        $("#slogan").text(sty) // 填充文字
        // alert("数据：" + data + "\n状态：" + status);
    });
    $.get("/a/backcolor/updates",function(data,status){
        var sty = data
        $("body").css("background-color",sty)// 修改背景颜色
        // alert("数据：" + data + "\n状态：" + status);
    });
    $.get("/a/fontsize/updates",function(data,status){
        var sty = String(data)+"px"
        $("#text").css("font-size",sty)// 修改时钟字体大小
        // alert("数据：" + data + "\n状态：" + status);
    });
    $.get("/a/slogan_fontsize/updates",function(data,status){
        var sty = String(data)+"px"
        $("#slogan").css("font-size",sty)// 修改标语字体大小
        // alert("数据：" + data + "\n状态：" + status);
    });

}


$(document).ready(function(){

    //保持样式为最新
    $.get("/a/slogan/updates",function(data,status){
        var sty = data
        $("#slogan").text(sty) // 填充文字
        // alert("数据：" + data + "\n状态：" + status);
    });
    $.get("/a/backcolor/updates",function(data,status){
        var sty = data
        $("body").css("background-color",sty)// 修改背景颜色
        // alert("数据：" + data + "\n状态：" + status);
    });
    $.get("/a/fontsize/updates",function(data,status){
        var sty = String(data)+"px"
        $("#text").css("font-size",sty)// 修改时钟字体大小
        // alert("数据：" + data + "\n状态：" + status);
    });
    $.get("/a/slogan_fontsize/updates",function(data,status){
        var sty = String(data)+"px"
        $("#slogan").css("font-size",sty)// 修改标语字体大小
        // alert("数据：" + data + "\n状态：" + status);
    });


  });


    doc.addEventListener('DOMContentLoaded', () => {
        const setters = {
            y: createTextSetter('#dy'),
            M: createTextSetter('#dm'),
            d: createTextSetter('#dd'),
            h: createTextSetter('#th'),
            m: createTextSetter('#tm'),
            s: createTextSetter('#ts'),
        }
        const timeMask = {
            d: doc.querySelector('#time>.mask.d>.p'),
            h: doc.querySelector('#time>.mask.h>.p'),
            m: doc.querySelector('#time>.mask.m>.p')
        };
        const updateText = () => {
            const now = new Date();
            setters.y(now.getFullYear());
            setters.M((now.getMonth() + 1).toFixed().padStart(2, '0'));
            setters.d(now.getDate().toFixed().padStart(2, '0'));
            setters.h(now.getHours().toFixed().padStart(2, '0'));
            setters.m(now.getMinutes().toFixed().padStart(2, '0'));
            if (setters.s(now.getSeconds().toFixed().padStart(2, '0'))) {
                timeMask.d.style.maxWidth = `${((now.getSeconds() + (60 * now.getMinutes()) + (3600 * now.getHours())) / 86400) * 100}%`;
                timeMask.h.style.maxWidth = `${((now.getSeconds() + (60 * now.getMinutes())) / 3600) * 100}%`;
                timeMask.m.style.maxWidth = `${(now.getSeconds() / 60) * 100}%`;
            }
        };
        setInterval(updateText, 1000);
        updateText();
    });
})(document);